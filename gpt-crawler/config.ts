import { Config } from "./src/config";

export const defaultConfig: Config = {
  url: "https://docs.gitlab.com/",
  match: "https://docs.gitlab.com/**",
  maxPagesToCrawl: 200,
  outputFileName: "output.json",
  maxTokens: undefined,
};
