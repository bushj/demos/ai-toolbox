# AI Toolbox

![AI Toolbox](./images/ai-toolbox.png)

### Links from presentation
* [LLM Colosseum](https://github.com/OpenGenerativeAI/llm-colosseum)
* [Introducing ChatGPT](https://openai.com/blog/chatgpt)
* [ChatGPT sets record for fastest-growing user base](https://www.reuters.com/technology/chatgpt-sets-record-fastest-growing-user-base-analyst-note-2023-02-01/)
* [Hugging Face](https://huggingface.co/models)
* NASA's [Prithvi](https://huggingface.co/ibm-nasa-geospatial/Prithvi-100M) model
* [GitLab Duo overview](https://about.gitlab.com/gitlab-duo/) and [documentation](https://docs.gitlab.com/ee/user/ai_features)
* Architecture of GitLab Duo's [AI-gateway](https://docs.gitlab.com/ee/architecture/blueprints/ai_gateway/)
* Andrej Karpathy: "[The hottest new programming language is English](https://x.com/karpathy/status/1617979122625712128?s=20)"
* Run AI models locally
  * [LM Studio](https://lmstudio.ai/) - download and run AI models
  * [Ollama](https://ollama.com/)
  * [Llama.cpp](https://github.com/ggerganov/llama.cpp)
* Examples of AI Gateways
  * [LiteLLM](https://litellm.ai/) - provides a common interface to all LLM APIs
  * [Portkey AI Gateway](https://github.com/portkey-ai/gateway)
  * [Cloudflare AI Gateway](https://github.com/portkey-ai/gateway)
  * [OpenRouter](https://openrouter.ai/)
* [GPTCache](https://github.com/zilliztech/GPTCache) - a library for creating a semantic cache for LLM queries
* [HuggingGPT](https://arxiv.org/abs/2303.17580)
* [GPTCrawler](https://github.com/BuilderIO/gpt-crawler)