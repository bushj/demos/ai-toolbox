curl https://api-inference.huggingface.co/models/bigcode/starcoder \
-X POST \
-H 'Content-Type: application/json' \
-H "Authorization: Bearer $HUGGINGFACE_API_KEY" \
-d '{"inputs":"Retrieve the first 100 characters from Google.com in JavaScript"}'
