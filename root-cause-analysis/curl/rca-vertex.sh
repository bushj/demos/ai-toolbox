cat << EOF > request.json
{
    "contents": [
        {
            "role": "user",
            "parts": [
                {
                    "text": "You are a brilliant DevOps expert, skilled in explaining the root cause of CI job failures. Below is the GitLab CI pipeline followed by the pipeline error. Think step by step and try to determine why the pipeline failed. Explain the failure and show an example fix.

image: docker:latest

services:
 - docker:dind

variables:
 CS_DEFAULT_BRANCH_IMAGE: $CI_REGISTRY_IMAGE/$CI_DEFAULT_BRANCH:$CI_COMMIT_SHA
 DOCKER_DRIVER: overlay2
 ROLLOUT_RESOURCE_TYPE: deployment
 DOCKER_TLS_CERTDIR: \\"\\" # https://gitlab.com/gitlab-org/gitlab-runner/issues/4501
 RUNNER_GENERATE_ARTIFACTS_METADATA: \\"true\\"
 CI_DEBUG_TRACE: \\"true\\"

stages:
 - build
 - test

include:
 - template: Jobs/Test.gitlab-ci.yml
 - template: Jobs/Container-Scanning.gitlab-ci.yml
 - template: Code-Quality.gitlab-ci.yml
 - template: Jobs/Dependency-Scanning.gitlab-ci.yml
 - template: Jobs/SAST.gitlab-ci.yml
 - template: Jobs/Secret-Detection.gitlab-ci.yml
 - template: Jobs/SAST-IaC.gitlab-ci.yml

build:
 stage: build
 variables:
  IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
 before_script:
  - apt list --installed
 script:
  - docker info
  - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  - docker build -t $IMAGE .
  - docker push $IMAGE

sast:
 variables:
  SEARCH_MAX_DEPTH: 12

apt list --installed
/bin/sh: eval: line 165: apt: not found"
                }
            ]
        }
    ],
    "generation_config": {
        "maxOutputTokens": 2048,
        "temperature": 0.9,
        "topP": 1
    },
    "safetySettings": [
        {
            "category": "HARM_CATEGORY_HATE_SPEECH",
            "threshold": "BLOCK_MEDIUM_AND_ABOVE"
        },
        {
            "category": "HARM_CATEGORY_DANGEROUS_CONTENT",
            "threshold": "BLOCK_MEDIUM_AND_ABOVE"
        },
        {
            "category": "HARM_CATEGORY_SEXUALLY_EXPLICIT",
            "threshold": "BLOCK_MEDIUM_AND_ABOVE"
        },
        {
            "category": "HARM_CATEGORY_HARASSMENT",
            "threshold": "BLOCK_MEDIUM_AND_ABOVE"
        }
    ]
}
EOF

API_ENDPOINT="us-central1-aiplatform.googleapis.com"
PROJECT_ID="jbush-d52d302f"
MODEL_ID="gemini-pro"
LOCATION_ID="us-central1"

curl \
-X POST \
-H "Authorization: Bearer $(gcloud auth print-access-token)" \
-H "Content-Type: application/json" \
"https://${API_ENDPOINT}/v1/projects/${PROJECT_ID}/locations/${LOCATION_ID}/publishers/google/models/${MODEL_ID}:streamGenerateContent" -d '@request.json'
